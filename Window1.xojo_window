#tag Window
Begin Window Window1
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   400
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   851509247
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Untitled"
   Visible         =   True
   Width           =   600
   Begin ProgressBar ProgressBar1
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Maximum         =   0
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      Top             =   334
      Value           =   0
      Visible         =   True
      Width           =   592
   End
   Begin PushButton PushButton1
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "OK"
      Default         =   True
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   287
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   378
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin TextArea TextArea1
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   True
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   275
      HelpTag         =   ""
      HideSelection   =   True
      Index           =   -2147483648
      Italic          =   False
      Left            =   31
      LimitText       =   0
      LineHeight      =   0.0
      LineSpacing     =   1.0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Multiline       =   True
      ReadOnly        =   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollbarVertical=   True
      Styled          =   True
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   37
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   542
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  dim f As  folderitem
		  f=GetFolderItem("Mirakl.log")
		  dim tis As TextInputStream
		  dim now As new date
		  tis = f.OpenAsTextFile
		  LogFile = f.AppendToTextFile
		  LogFile.WriteLine str(now)
		  dim prefsParam, settingsParam,dateparam as String
		  dim commandline() as String
		  dim prefsNew As new JSONItem
		  dim settingsNew As new JSONItem
		  prefs=prefsNew
		  settings=settingsNew
		  '#if  not DebugBuild
		  iF InStr(System.CommandLine, "^") >0 then
		    commandline()=split(System.CommandLine,"^")
		    prefsParam=commandline(1)
		    prefs.Load(prefsParam)
		    'MsgBox (prefs.ToString)
		    settingsParam=commandline(2)
		    settings.Load(settingsParam)
		    dateparam=commandline(3)
		    createdAfter=NthField(dateparam,",",1)
		    createdBefore=NthField(dateparam,",",2)
		    'MsgBox System.CommandLine
		    if commandline.Ubound =4 then
		      operation=commandline(4)
		    end if
		    
		  end if
		  '#Else
		  //createdBefore="2021-06-18T23:59:59Z"
		  //createdAfter="2021-06-17T00:00:00Z"
		  '#endif
		  #if DebugBuild then
		    operation="r"
		  #endif
		  'LogFile.WriteLine prefs.ToString
		  'LogFile.WriteLine settings.ToString
		  LogFile.WriteLine dateparam
		  
		  GetMiraklOrders
		  'if operation="r" then
		  'Window1.SpreeSocket1.GetCustomerReturns
		  'Else
		  'Window1.SpreeSocket1.GetSpreeOrders
		  'end if
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub GetMiraklOrders()
		  // OR11 - List orders
		  dim fl As FolderItem
		  dim tis as TextInputStream
		  dim appK As String
		  fl=GetFolderItem ("mirakl.mkp")
		  //tis =TextInputStream.Open f.a
		  tis = fl.OpenAsTextFile
		  appK=tis.ReadAll
		  
		  //msgbox (appK)
		  // Set up the socket
		  dim h as new HTTPSecureSocket
		  h.Secure = True
		  h.ConnectionType = h.TLSv12
		  h.setRequestHeader("Authorization",appk)
		  h.setRequestHeader("Accept","application/json")
		  //d31433f7-780f-40cf-98e9-6e1b35a022b9
		  // Set the URL
		  //dim url as string = "https://maisonette-prod.mirakl.net/api/orders?start_date=2021-08-24T08:36:15&end_date=2021-08-27T08:36:15&max=2"
		  dim url as string = "https://maisonette-prod.mirakl.net/api/orders?max=100&start_date=" + createdAfter +"&end_date=" +  createdBefore 
		  
		  // Send Synchronous Request
		  dim s as string = h.SendRequest("GET",url,30)
		  TextArea1.Text=s
		  'Break
		  dim pageNumber As Auto
		  dim err  As String
		  err =str( h.ErrorCode)
		  Dim jsonDict As new JSONItem
		  Select Case err
		  case "200","201","0"
		    
		    jsonDict.Load(s)
		    //Dim jsonDict As Xojo.Core.Dictionary =json jsonSet. Content.ConvertJSONToDictionary
		    
		    
		    if jsonDict.Value("total_count")="0" then
		      MsgBox "No Orders to process"
		      quit 
		    Else
		      dim count As Integer
		      count=jsonDict.Value("total_count")
		      LogFile.WriteLine str(count) + " Transactions returned"
		    end if
		  case else
		    Break
		    MsgBox err
		  End Select
		  dim jpage As  new JSONItem
		  dim pages As  new JSONItem
		  jpage.Value("page1")=jsonDict
		  pages.Append(jpage)
		  dim json as text = pages.ToString.ToText
		  dim f As FolderItem
		  'dim f As FolderItem
		  f=GetFolderItem("miraklorders.json")
		  dim tos As  TextOutputStream= TextOutputStream.Create(f)
		  tos.Write  json
		  processMiraklData
		  Break
		  Quit
		  
		  ''//SpreeData.Append(jsonDict)
		  'if IsNull(SpreeDataPages) then SpreeDataPages=  new xojo.Core.Dictionary
		  'dim numpages as Integer =jsonDict.Value("pages") 
		  'if numpages >=1 then
		  ''//me.pgnum=val(jsonDict.Value("current_page"))
		  ''
		  ''pageNumber=jsonDict.Value("current_page")
		  ''if IsNumeric(pageNumber) then
		  ''me.pgnum=pageNumber
		  ''Else
		  ''me.pgnum=val(pageNumber)
		  ''end if
		  'if jsonDict.HasKey("orders") Then spreeDataPages.Value("page"+str(me.pgnum))=jsonDict.Value("orders")
		  ''if jsonDict.HasKey("credit_memos") Then spreeDataPages.Value("page"+str(me.pgnum))=jsonDict.Value("credit_memos")
		  ''
		  'SpreeData.Append(SpreeDataPages)
		  'pgnum=pgnum +1 
		  'self.page=pgnum.ToText
		  'if me.pgnum <= numpages then
		  'xojo.Core.Timer.CallLater(0, AddressOf GetSpreeOrders)
		  'Else
		  'break
		  'dim json as text = Xojo.Data.GenerateJSON( SpreeData )
		  'dim f As FolderItem
		  'f=GetFolderItem("spreeorders.json")
		  'dim tos As  TextOutputStream= TextOutputStream.Create(f)
		  'tos.Write  json
		  'processSpreeData
		  'Quit
		  'end if
		  
		  //end if
		  
		  'LogFile.WriteLine HTTPStatus.ToText 
		  'MsgBox HTTPStatus.ToText 
		  dim errtxt As Text
		  //errtxt = Xojo.Core.TextEncoding.UTF8.ConvertDataToText(content)
		  LogFile.WriteLine  errtxt
		  MsgBox errtxt
		  
		  quit
		  
		  'End Select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub processMiraklData()
		  dim launchParam As String
		  dim FileEditor as string
		  dim FProg as FolderItem
		  FileEditor = "qbupdater.exe"
		  //FileEditor = "P:\Development\qbupdater_git\Debugqbupdater\Debugqbupdater.exe"
		  //FileEditor = "C:\Users\tkvmmac\AppData\Local\Temp\Debugqbupdater\Debugqbupdater.exe"
		  launchParam="^" + (Prefs.ToString )  + "^" + (Settings.ToString) + "^"  +"File|miraklorders.json"  + "^" + operation
		  'MsgBox (launchParam)
		  FProg = GetFolderItem(FileEditor)
		  FProg.Launch(launchParam)
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		commandline As String
	#tag EndProperty

	#tag Property, Flags = &h0
		createdAfter As String
	#tag EndProperty

	#tag Property, Flags = &h0
		createdBefore As String
	#tag EndProperty

	#tag Property, Flags = &h0
		LogFile As TextOutputStream
	#tag EndProperty

	#tag Property, Flags = &h0
		MiraklData As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		operation As String
	#tag EndProperty

	#tag Property, Flags = &h0
		page As Text = "1"
	#tag EndProperty

	#tag Property, Flags = &h0
		pages As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		pgnum As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		prefs As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		settings As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		SpreeDataPages As Xojo.Core.Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		spreeFile As Xojo.IO.FolderItem
	#tag EndProperty


#tag EndWindowCode

#tag Events PushButton1
	#tag Event
		Sub Action()
		  GetMiraklOrders
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="commandline"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Group="OS X (Carbon)"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="createdAfter"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="createdBefore"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Frame"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Frame"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Group="OS X (Carbon)"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Menus"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Deprecated"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="operation"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="page"
		Group="Behavior"
		InitialValue="1"
		Type="Text"
	#tag EndViewProperty
	#tag ViewProperty
		Name="pages"
		Group="Behavior"
		InitialValue="-1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="pgnum"
		Group="Behavior"
		InitialValue="-1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Frame"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
